FROM debian:bookworm as building_package
WORKDIR /build
COPY src/ /build
RUN apt update && apt install -y cmake clang libssl-dev libboost-filesystem-dev libboost-thread-dev
RUN cmake -H. -Bbuild -DBUILD_TESTING=ON -DBUILD_FUZZING=ON
RUN cmake --build build


