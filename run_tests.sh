mkdir -p /opt/src
cp -rf /build-out/* /opt/src/
cd /opt/src/build/tests
declare -a codes=()
exc=0
test_pattern="*_test"
for filename in *; do
    if [[ -f $filename && -x $filename ]]; then
        echo "Running $filename"
        if [[ $filename == $test_pattern ]]; then
            if ! eval "./$filename"; then
                exc=1
            fi
        else
            if ! eval "./$filename -runs=100000"; then
                exc=1
            fi
        fi
    fi
done

exit $exc

