#apt update && apt install -y cmake clang libssl-dev libboost-filesystem-dev libboost-thread-dev
mkdir /opt/src  && cp -rf /build-out/* /opt/src/
$CODEQL_HOME/codeql/codeql database create --overwrite  --language=cpp /opt/results/source_db -s /opt/src
$CODEQL_HOME/codeql/codeql database analyze --format=sarif-latest --output=/opt/results/issues.sarif /opt/results/source_db cpp-security-and-quality.qls
